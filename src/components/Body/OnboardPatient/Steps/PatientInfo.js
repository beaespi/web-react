import React from 'react';

// Images
import inviteWhite from '../../../../assets/invite-white.svg';

export default class PatientInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fieldsCompleted: 0,
      allFields: 6
    };
  }

  // Specific fields for each page
  handleIsCompleted = (fields) => {
    let enable = false;
    if (fields === (this.state.allFields)) {
      enable = true;
    }
    this.props.onEnable(enable);
  }

  // Specific fields for each page
  handleNumberOfFields = (e, prev) => {
    let fields = this.state.fieldsCompleted;
    const {name, value} = e.target
    let patientName = name;
    if (patientName === 'firstName' || 
        patientName === 'lastName' || 
        patientName === 'email' || 
        patientName === 'bday' || 
        patientName === 'gender' || 
        patientName === 'language') {
      if (value === '' || value === 'None') {
        fields -= 1;
      } else if (prev === '' || prev === 'None') {
        fields += 1;
      }
      this.setState({
        fieldsCompleted: fields
      });
      this.handleIsCompleted(fields);
    }
  }

  render() {
    if (this.props.currentStep !== 2) {
      return null
    }
    
    return (
      <div role="contentinfo">
        <h4 className="grey lh-1-10 mt-0 mb-xxxxxxl">
          Patient
        </h4>
        <p className="grey overline lh-1-65 mt-0 mb-xxs ls-1-5">Step 1</p>
        <div className="bg-xxxx-light-blue p-small br-xxxs flex align-items-center">
          <img src={inviteWhite}
               className="h-fixed-50"
               alt="add patient information" />
          <p className="pl-lrg m-0 fs-small fw-regular grey lh-1-25">
            If you want to onboard multiple patients, please contact Tactio Support for assistance.
          </p>
        </div>
        <div variant="formGroup"
             role="group" 
             className="flex flex-wrap justify-between mt-xxxxl">
          <label htmlFor="firstName"
                 className="max-w-fixed-330 w-100 mb-lrg">
            Patient name *
            <input type="text" 
                   placeholder="Patient name"
                   name="firstName" 
                   id="firstName"
                   className="w-100 block"
                   onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.firstName)}}
                   value={this.props.firstName}
                   required />
          </label>
          <label htmlFor="lastName"
                 className="max-w-fixed-330 w-100 mb-lrg">
            Patient last name *
            <input type="text" 
                   placeholder="Patient last name"
                   name="lastName"
                   id="lastName"
                   className="w-100 block"
                   onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.lastName)}}
                   value={this.props.lastName}
                   required />
          </label>  
          <label htmlFor="email"
                 className="max-w-fixed-330 w-100 mb-lrg">
            Email *
            <input type="email" 
                   placeholder="Patient email"
                   name="email" 
                   id="email"
                   className="w-100 block"
                   onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.email)}}
                   value={this.props.email}
                   required />
          </label>
          <label htmlFor="mobilePhone"
                 className="max-w-fixed-330 w-100 mb-lrg">
            Cell phone number
            <input type="text" 
                   placeholder="456-456-7899"
                   name="mobilePhone" 
                   id="mobilePhone"
                   className="w-100 block" 
                   onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.mobilePhone)}}
                   value={this.props.mobilePhone} />
          </label> 
          <label htmlFor="bday"
                 className="max-w-fixed-330 w-100 mb-lrg">
            Date of birth*
            <input type="date" 
                   placeholder="YYYY/MM/DD"
                   name="bday" 
                   id="bday"
                   className="w-100 block"
                   onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.bday)}}
                   value={this.props.bday}
                   required />
          </label> 
          <label htmlFor="mrn"
                 className="max-w-fixed-330 w-100 mb-lrg">
            MRN
            <input type="text" 
                   placeholder="Patient MRN"
                   name="mrn" 
                   id="mrn"
                   onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.mrn)}}
                   value={this.props.mrn}
                   className="w-100 block" />
          </label>          
          <label className="fs-small grey lh-1-25 max-w-fixed-330 w-100">
            Gender*
            <select className="select-styled mt-xxs w-100 grey" 
                    name="gender" 
                    onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.gender)}}
                    value={this.props.gender}
                    required>
              <option value="None"
                      disabled
                      hidden>
                Select gender
              </option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          </label>
          <label className="fs-small grey lh-1-25 max-w-fixed-330 w-100">
            Language*
            <select className="select-styled mt-xxs w-100 grey" 
                    name="language" 
                    onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.language)}}
                    value={this.props.language}
                    required>
              <option value="None"
                      disabled
                      hidden>
                Language
              </option>
              <option value="English">English</option>
              <option value="French">French</option>
            </select>
          </label> 
        </div>
      </div>
    )
  } 
}