import React from 'react';

export default class SingleCarePlan extends React.Component {

  render() {
   return ( 
      <div className={`accordion-checkbox br-xxxs p-small bg-xxx-light-grey box-shadow-soft max-w-fixed-330 w-100 border-box mb-xxs ${this.props.isSelected ? "border-blue": ""}`}>
        <input type="checkbox"
               id={this.props.name}
               className="mr-xxxs"
               name={this.props.name}
               value={this.props.name}
               onChange={this.props.onSelect}
               checked={this.props.isSelected}/>      
        <label htmlFor={this.props.name}
               className="accordion-checkbox-heading fs-small fw-semibold lh-1-25 grey inline-block w-100">
          {this.props.name}
        </label>
        <ul className="accordion-checkbox-content my-0 fs-h-xs lh-1-45">
          {this.props.availableFeatures.map((feature, key) => {
            return <li key={key}>{feature}</li>
          })}
          {this.props.unavailableFeatures.map((feature, key) => {
            return <li key={key} className="empty-li strike light-grey ml-xxxs">{feature}</li>
          })}
        </ul>
      </div>
    )
  }
}