import React from 'react';

// Components
import CarePlan from './SingleCarePlan'

// Images
import checklist from '../../../../../assets/checklist.svg';

export default class CarePlans extends React.Component {
  render() {
    if (this.props.currentStep !== 3) {
      return null
    }   

    return (
      <div role="contentinfo">
        <h4 className="grey lh-1-10 mt-0 mb-xxxxxxl">
          Care Plan
        </h4>
        <p className="grey overline lh-1-65 mt-0 mb-xxs ls-1-5">Step 2</p>
        <div className="bg-xxxx-light-blue p-small br-xxxs flex align-items-center">
          <img src={checklist}
               className="h-fixed-50"
               alt="Checklist information" />
          <p className="pl-lrg m-0 fs-small fw-regular grey lh-1-25">
            Select a care plan(s). Note: Care plans can be added/edited once the patient is onboarded as well.
          </p>
        </div> 
        <div className="flex flex-wrap justify-between align-items-start mt-xxxxl">
          {this.props.carePlans.map((carePlan) => {
            return <CarePlan key={carePlan.id}
                             isSelected={carePlan.isSelected}
                             onSelect={this.props.onSelect}
                             name={carePlan.name}
                             availableFeatures={carePlan.availableFeatures}
                             unavailableFeatures={carePlan.unavailableFeatures} />
            })
          }
        </div>
      </div>
    )
  }
}