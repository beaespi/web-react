import React from 'react';


export default class Device extends React.Component {

  render() {
  	if (this.props.currentStep === 4){
  		// Medical Devices page
  		return (
  			<table className="w-100 mt-small">
	        	<tbody>
		        	<tr>
		        		<td className="py-lrg w-fixed-80">
		        			<img className="max-w-fixed-80 w-100" 
		        			     src={this.props.img}
		        			     alt={this.props.name} />
		        		</td>
		        		<td className="pl-lrg pr-xxs w-fixed-300">
		        			<p className="my-0 body-2 lh-1-25 mb-xxxs">
		        				{this.props.name}
		        			</p>
		        			<span className="body-3 light-grey">
		        				{this.props.description}
		        			</span>
		        		</td>
		        		<td>
		        			<button type="button"
		        							className="bg-light-green br-pill btn-info caption ls-0-4 sentence lh-1-55">
		        				Cell-based device
		        			</button>
		        		</td>
		        		<td className="input-container align-right">
		        			<input type="checkbox" 
		        						 id={this.props.name}
		        						 name={this.props.name}
		        						 onChange={this.props.onSelect}
		        						 checked={this.props.isSelected}/>
		        			<label htmlFor={this.props.name}
		        						 className="input-label-unspaced pos-rel" />
		        		</td>
		        	</tr>
		        </tbody>       	
	        </table>

  		);
  	} else {
  		// Shipping and confirmation pages
  		return (
	  		<div className="bg-xxxx-light-blue p-small br-xxxs flex align-items-center max-w-fixed-330 w-100 border-box">
					<img className="max-w-fixed-80 w-100" 
					     src={this.props.img}
					     alt={this.props.name} />
					<div className="pl-small">
		  			<p className="my-0 body-2 lh-1-25 mb-xxxs">
		  				{this.props.name}
		  			</p>
		  			<span className="body-3 light-grey">
		  				{this.props.description}
		  			</span>								
					</div>   
		  	</div>
  		);
  	}
  }
 }