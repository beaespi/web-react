import React from 'react';

// Components
import Device from './SingleDevice'

// Images
import icon4G from '../../../../../assets/4g.svg';

export default class Devices extends React.Component {

  render() {
  	// TODO: switch table with another mark-up
  	if (this.props.currentStep !== 4) {
      return null
    }
    return (
        <div role="rowgroup">

          <h4 className="grey lh-1-10">
            Medical Devices
          </h4>
	        <p className="grey overline lh-1-65 mt-0 mb-xxs ls-1-5">Step 3</p>
	        <div className="bg-xxxx-light-blue p-small br-xxxs flex align-items-center">
	        	<img src={icon4G}
	        			 alt="4G icon"
	               className="h-fixed-50" />
	          <p className="pl-lrg m-0 fs-small fw-regular grey lh-1-25">
	          	These 4G-enabled medical devices will be pre-configured and shipped directly to your patient’s home. Skip if using only Bluetooth devices that are set up at your medical practice.
	          </p>
	        </div>

	        {this.props.devices.map((device) => {
    				return <Device key={device.id}
    											isSelected={device.isSelected}
    											onSelect={this.props.onSelect}
    											currentStep={this.props.currentStep}
    											name={device.name}
													img={device.img}
													description={device.description} />
						})
	      	} 
        </div>
    )
  }
}
 