import React from 'react';

// Images
import validation from '../../../../assets/Validation.svg';
import question from '../../../../assets/question.svg';

export default class FinalizedOrder extends React.Component {
  
  render() {
    // Handle steps for navbar
    if (this.props.currentStep !== 7) {
      return null
    }

    // Things to show if inviteVal === inviteWithDevice
    let message = null;
    let shippingConfirmation = null;
    if (this.props.isFullSteps) {
      message = 'Your order has been placed!';
      shippingConfirmation = (
       <p className="mt-0 mb-xxxxxxl fs-small fw-regular grey lh-1-24">
          {this.props.firstName + ' ' + this.props.lastName} will receive the device(s) in the next few days and you will receive an email with a tracking number.
       </p>
      )
    } else {
      message = 'Your invitation has been sent!'
    }

    // Return page content
    return (
      <div role="contentinfo"
         className="align-center">
        <img src={validation}
    		  	 alt="Validation Check"
             className="w-fixed-135" />
    		<h4 className="grey lh-1-10 mt-xs w-fixed-265 mb-xxxxl ml-auto mr-auto">
      		{message}
    		</h4>
    		{shippingConfirmation}
    		<div className="flex flex-wrap align-items-center justify-center">
          <img src={question}
      		  	 alt="Confirmation information"
               className="h-fixed-24 w-fixed-24 mr-small" />
          <p className="m-0 fs-small fw-regular grey lh-1-24 mt-auto">
            If you do not receive a confirmation email within the next 24 hours please <a href="https://www.tactiohealth.com/en/contact-us" className="blue no-decor">contact us</a>.
          </p>
      	</div>
      </div>
    )
  }
}