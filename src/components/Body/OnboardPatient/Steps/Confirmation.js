import React from 'react';

// Components
import Device from '../Steps/Devices/SingleDevice'

// Images 
import shippingIcon from '../../../../assets/shipping-icon.svg';
import pin from '../../../../assets/pin.svg';
import phone from '../../../../assets/phone.svg';
import calendar from '../../../../assets/calendar.svg';
import greenCheck from '../../../../assets/check-green.svg';
import business from '../../../../assets/business.svg';
import personAdd from '../../../../assets/person-add.svg';


export default class Confirmation extends React.Component {

  render() {
    if (this.props.currentStep !== 6) {
      return null
    } 

    let order = null;
    let shipping = null;

    if (this.props.isFullSteps) {
      order = (
        <div>
        <div className="flex flex-wrap align-items-center mt-xxxl">
          <img src={shippingIcon}
               className="h-fixed-24 w-fixed-24 mr-small"
               alt="Shipping icon" />
          <p className="m-0 fs-small fw-bold grey lh-1-20 mt-xxxxs uppercase">
            Order
          </p>
        </div>
        <div className="flex flex-wrap justify-between mt-xxxxl">
        {this.props.devices.map((device, key) => {
          if (device.isSelected) {
            return <Device  key={key}
                            name={device.name}
                            img={device.img}
                            description={device.description} />
          } else {
            return null
          } 
        })} 
      </div>
        </div>
      );

      shipping = (
        <div>
          <div className="flex flex-wrap align-items-start mt-xxxl">
          <img className="h-fixed-24 w-fixed-24 mr-small" 
               src={pin} 
               alt="Pin icon" />
          <div role="contentinfo">
            <p className="m-0 fs-small fw-bold grey lh-1-20 mt-xxxxs uppercase">
              Shipping address
            </p>
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto capitalize">
              {this.props.shippingFirstName} {this.props.shippingLastName}
            </p>
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto capitalize">
              {this.props.street}, {this.props.city} <br />
              {this.props.state}, {this.props.country} <br />
              {this.props.postalCode} 
            </p>
          </div>
          </div>
          <div className="flex flex-wrap align-items-start mt-xxxl">
            <img className="h-fixed-24 w-fixed-24 mr-small" 
                 src={phone} 
                 alt="Phone icon" />
            <div role="contentinfo">
              <p className="m-0 fs-small fw-bold grey lh-1-20 mt-xxxxs uppercase">
                Contact info
              </p>
              <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto capitalize">
                {this.props.countryCode}{this.props.shippingPhone} 
              </p>
            </div>
          </div>
          <div className="flex flex-wrap align-items-start mt-xxxl">
            <img className="h-fixed-24 w-fixed-24 mr-small" 
                 src={calendar} 
                 alt="Calendar icon" />
            <div role="contentinfo">
              <p className="m-0 fs-small fw-bold grey lh-1-20 mt-xxxxs uppercase">
                Shipping preferences
              </p>
              <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto sentence p-lrg pl-0">
                {this.props.deliveryPreference} 
              </p>
            </div>
          </div>
        </div>);
    }

    return (
      <div role="contentinfo">
        <h4 className="grey lh-1-10 mt-0 mb-xxxxxxl">
          Confirmation
        </h4>
        <p className="grey overline lh-1-65 mt-0 mb-xxs ls-1-5">Last step</p>
        <div className="bg-xxxx-light-blue p-small br-xxxs flex align-items-center">
          <img src={greenCheck}
               className="h-fixed-50"
               alt="Green Check" />
          <p className="pl-lrg m-0 fs-small fw-regular grey lh-1-25">
           Please, confirm all the information before sending the invitation.
          </p>
        </div>
        <div className="flex flex-wrap align-items-start mt-xxxxxxl">
          <img src={business}
               className="h-fixed-24 w-fixed-24 mr-small"
               alt="Business icon" />
          <div content="heading">
            <p className="m-0 fs-small fw-bold grey lh-1-20 mt-med uppercase">
              Group
            </p>
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto">
              {this.props.group}
            </p>
          </div>
        </div>
        <div className="flex flex-wrap align-items-center mt-xxxl">
          <img src={personAdd}
               className="h-fixed-24 w-fixed-24 mr-small" 
               alt="Add a person icon"/>
          <div content="heading">
            <p className="m-0 fs-small fw-bold grey lh-1-20 mt-xxxxs uppercase">
              Patient
            </p>
          </div>
        </div>
        <div className="flex flex-wrap align-items-start justify-between">
          <div role="contentinfo"
               className="pl-xxxxl">
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto ml-xxxxl capitalize">
              {this.props.firstName + ' ' + this.props.lastName}
            </p>
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto ml-xxxxl lowecase">
              {this.props.email}
            </p>
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto ml-xxxxl">
              {this.props.bday}
            </p>
          </div>
          <div role="contentinfo">
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto">
              {this.props.mobilePhone}
            </p>
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto capitalize">
              {this.props.gender}
            </p>
            <p className="m-0 fs-small fw-regular grey lh-1-20 mt-auto capitalize">
              {this.props.language}
            </p>
          </div>
          <div role="contentinfo">
            <p className="m-0 fs-small fw-bold grey lh-1-20 mt-auto capitalize">
              Care plan
            </p>
            {this.props.carePlans.map((carePlan, key) => {
                return carePlan.isSelected ?
                  <p  key={key} 
                      className="m-0 fs-small fw-regular grey lh-1-20 mt-auto capitalize">
                    {carePlan.name}
                  </p> :
                  null
            })}
            
          </div>
        </div>

        {order}
        {shipping}

      </div>
    )

  }
}