import React from 'react';

// Components
import Device from '../Steps/Devices/SingleDevice'

// Images
import pin from '../../../../assets/pin.svg';
import phone from '../../../../assets/phone.svg';
import calendar from '../../../../assets/calendar.svg';

export default class ShippingInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: false,
      fieldsCompleted: 0,
      allFields: 7
    };
  }

  handleCheck = (e) => {
    this.props.onChange(e); // update the delivery preference state
    this.setState({
      isChecked: !this.state.isChecked // update checkbox status
    });
  }

  // Specific fields for each page
  handleIsCompleted = (fields) => {
    let enable = false;
    if (fields === (this.state.allFields)) {
      enable = true;
    }
    this.props.onEnable(enable);
  }

  // Specific fields for each page
  handleNumberOfFields = (e, previous) => {
    let fields = this.state.fieldsCompleted;
    let shippingName = e.target.name;
    if (shippingName === 'shippingFirstName' || 
    		shippingName === 'shippingLastName' || 
      	shippingName === 'street' || 
      	shippingName === 'city' || 
      	shippingName === 'state' || 
      	shippingName === 'country' || 
      	shippingName === 'postalCode') {
      if (e.target.value === ''){
        fields -= 1;
      } else if (previous === '') {
        fields += 1;
      }
      if (fields > this.state.allFields) { // wrap around
        fields = (this.state.allFields);
      }
      this.setState({
        fieldsCompleted: fields
      });
      this.handleIsCompleted(fields);
    }
  }

  render() {
  	// TODO: adjust selected devices to link accordingly
    if (this.props.currentStep !== 5) {
      return null
    }  

    let checkBoxValue = "Don't deliver on weekends";

    if (this.state.isChecked) {
      checkBoxValue = "Deliver anytime";
    } 

    let devices = (
    	<div className="flex flex-wrap justify-between mt-xxxxl">
    		{this.props.devices.map((device) => {
    			if (device.isSelected) {
    				return <Device key={device.id}
    											name={device.name}
													img={device.img}
													description={device.description} />
					} else return null
				})} 
    	</div>
    );

    return (
        <div role="rowgroup">
          <h4 className="grey lh-1-10">
            Shipping Information
          </h4>
	        <p className="grey overline lh-1-65 mt-0 mb-xxs ls-1-5">Step 4</p>

	        {devices}

	        <p className="uppercase subtitle-1 lh-1-25 fw-semibold mb-0 mt-xxxxl">
	        	<span className="mr-xs">
	        		<img src={pin}
	        			   alt="4G icon"
	               	 className="h-fixed-20 v-align-top" />
	        	</span>
	        	Shipping Address
	        </p>
	        <div role="group" 
	        		 variant="formGroup"
	        		 className="flex flex-wrap justify-between mt-h-xxl">
	        	<label htmlFor="firstName"
	        				 className="max-w-fixed-330 w-100 mb-lrg">
	        		First name *
	        		<input type="text" 
	        					 placeholder="First name"
	        					 name="shippingFirstName" 
	        					 id="firstName"
	        					 className="w-100 block"
	        					 onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.shippingFirstName)}}
	        					 value={this.props.shippingFirstName}
	        					 required />
	        	</label>
	        	<label htmlFor="lastName"
	        				 className="max-w-fixed-330 w-100 mb-lrg">
	        		Last name *
	        		<input type="text" 
	        					 placeholder="Last name"
	        					 name="shippingLastName" 
	        					 id="lastName"
	        					 className="w-100 block"
	        					 onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.shippingLastName)}}
	        					 value={this.props.shippingLastName}
	        					 required />
	        	</label>
	        	<label htmlFor="street"
	        				 className="max-w-fixed-330 w-100 mb-lrg">
	        		Street *
	        		<input type="text" 
	        					 placeholder="Street"
	        					 name="street" 
	        					 id="street"
	        					 className="w-100 block"
	        					 onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.street)}}
	        					 value={this.props.street}
	        					 required />
	        	</label>
	        	<label htmlFor="city"
	        				 className="max-w-fixed-330 w-100 mb-lrg">
	        		City *
	        		<input type="text" 
	        					 placeholder="City"
	        					 name="city" 
	        					 id="city"
	        					 className="w-100 block"
	        					 onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.city)}}
	        					 value={this.props.city}
	        					 required />
	        	</label>	  
	        	<label htmlFor="country"
	        				 className="max-w-fixed-330 w-100">
	        		Country *
	        		<input type="text" 
	        					 placeholder="Country"
	        					 name="country" 
	        					 id="country"
	        					 className="w-100 block"
	        					 onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.country)}}
	        					 value={this.props.country}
	        					 required />
	        	</label>
	        	<label htmlFor="state"
	        				 className="max-w-fixed-330 w-100 mb-lrg">
	        		State/Province *
	        		<input type="text" 
	        					 placeholder="State/Province"
	        					 name="state" 
	        					 id="state"
	        					 className="w-100 block"
	        					 onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.state)}}
	        					 value={this.props.state}
	        					 required />
	        	</label>	
	        	<label htmlFor="postalCode"
	        				 className="max-w-fixed-330 w-100">
	        		Zip code *
	        		<input type="text" 
	        					 placeholder="Zip code"
	        					 name="postalCode" 
	        					 id="postalCode"
	        					 className="w-100 block"
	        					 onChange={(e) => {this.props.onChange(e); this.handleNumberOfFields(e, this.props.postalCode)}}
	        					 value={this.props.postalCode}
	        					 required />
	        	</label>
	        </div>

	        <p className="uppercase subtitle-1 lh-1-25 fw-semibold mb-0 mt-xxxxl">
	        	<span className="mr-xs">
	        		<img src={phone}
	        			   alt="4G icon"
	               	 className="h-fixed-20 v-align-top" />
	        	</span>
	        	Contact info
	        </p>
	        <div role="group" 
	        		 variant="formGroup"
	        		 className="flex flex-wrap mt-h-xxl">
	        	<label htmlFor="countryCode"
	        				 className="max-w-fixed-40 w-100 mr-small">
	        		Prefix
	        		<input type="text" 
	        					 placeholder="+1"
	        					 name="countryCode" 
	        					 id="countryCode"
	        					 className="w-100 block"
	        					 onChange={this.props.onChange}
	        					 value={this.props.countryCode}
	        					 required />
	        	</label>

	        	<label htmlFor="tel"
	        				 className="max-w-fixed-330 w-100">
	        		Phone number
	        		<input type="text" 
	        					 placeholder="514-444-5555"
	        					 pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
	        					 name="shippingPhone" 
	        					 id="tel"
	        					 className="w-100 block"
	        					 onChange={this.props.onChange}
	        					 value={this.props.shippingPhone}
	        					 required />
	        	</label>
	        </div>

	        <p className="uppercase subtitle-1 lh-1-25 fw-semibold mb-0 mt-xxxxl">
	        	<span className="mr-xs">
	        		<img src={calendar}
	        			   alt="4G icon"
	               	 className="h-fixed-20 v-align-top" />
	        	</span>
	        	Shipping Preferences
	        </p>
	        <div role="group" 
	        		 variant="formGroup"
	        		 className=" mt-h-xxl input-container">
      			<input type="checkbox" 
      						 id="noWeekendDelivery"
      						 name="deliveryPreference"
      						 value={checkBoxValue}
      						 onChange={(e) => this.handleCheck(e)}
      						 checked={this.state.isChecked}/>
						<label htmlFor="noWeekendDelivery"
									 className="input-label pos-rel">
							Don't deliver on weekends
						</label>
					</div>
        </div>
    )
  }
}