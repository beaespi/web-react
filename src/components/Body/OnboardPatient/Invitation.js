import React from 'react';

// Images
import invite from '../../../assets/invite.svg';
import bluetooth from '../../../assets/bluetooth.svg';

export default class Invitation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteValue: '',
    }
  }

  handleChange = (e) => {
    const {name, value} = e.target
    this.setState({
      [name]: value
    });
    this.props.onInvite(value);
    this.handleIsCompleted(e);

  }

  // Specific fields for each page
  handleIsCompleted = (e) => {
    let enable = false;
    let name = e.target.name;
    if (name === 'group' && (this.props.inviteBy !== '')){
      enable = true;
    } else if (name === 'inviteValue' && (this.props.group !== 'Select a group')) {
      enable = true;
    }
    this.props.onEnable(enable);
  }

  render() {
    if (this.props.currentStep !== 1) {
      return null
    }    

    return (
      <section role="contentinfo"
               className="pl-h-xxxl">
        <h4 className="grey lh-1-10 mt-0 mb-xxxxxxl">
          Let's onboard your patient
        </h4>
        <label className="fs-small fw-semibold grey lh-1-25">
          Choose your group
          <select className="mt-xxs max-w-fixed-330 w-100"
                  name="group" 
                  onChange={(e) => {this.props.onGroup(e); this.handleIsCompleted(e)}}
                  value={this.props.group}>
            <option value="Select a group"
                    disabled
                    hidden>
              Select group
            </option>
            <option value="Medicare clinic">Medicare clinic</option>
            <option value="Hospital">Hospital</option>
            <option value="Pharmacy">Pharmacy</option>
            <option value="Government">Government</option>
          </select>
        </label>
        <div className="mt-xxxxxxl">
          <p className="fs-small fw-semibold grey lh-1-25">
            How would you link to onboard your patient?
          </p>
          <div className="flex flex-wrap align-items-center py-lrg bb-xx-light-grey">
            <input type="radio" 
                   id="inviteOnly"
                   name="inviteValue"
                   value="inviteOnly"
                   onChange={(e) => {this.handleChange(e); this.handleIsCompleted(e)}}
                   checked={this.state.inviteValue === "inviteOnly"}
                   />
            <label htmlFor="inviteOnly"
                   className="flex flex-wrap align-items-center">
              <div className="pl-med pr-lrg">
                <img src={invite}
                     className="h-fixed-50"
                     alt="Invite a patient" />
              </div>
              <dl className="m-0 max-w-fixed-520">
                <dt className="fs-small fw-semibold grey lh-1-25 mb-xxxs">Invite only</dt>
                <dd className="m-0 fs-small fw-regular grey lh-1-25">
                  Send your patient an invite only. Choose this if you plan on using Bluetooth devices or no devices.
                </dd>
              </dl>
            </label>
          </div>
          <div className="flex flex-wrap align-items-center py-lrg">
            <input type="radio" 
                   id="inviteWithDevice"
                   name="inviteValue"
                   value="inviteWithDevice"
                   onChange={(e) => {this.handleChange(e); this.handleIsCompleted(e)}}
                   checked={this.state.inviteValue === "inviteWithDevice"} />
            <label htmlFor="inviteWithDevice"
                   className="flex flex-wrap align-items-center">
              <div className="pl-med pr-lrg">
                <img src={bluetooth}
                     className="h-fixed-50"
                     alt="Invite a patient with a device" />
              </div>
              <dl className="m-0 max-w-fixed-520">
                <dt className="fs-small fw-semibold grey lh-1-25 mb-xxxs">Invite & order 4G devices</dt>
                <dd className="m-0 fs-small fw-regular grey lh-1-25">
                  Send your patient an invite and select 4G medical devices to be sent to your patient directly.
                </dd>
              </dl>
            </label>
          </div>
        </div>
      </section>
    )
  }
}
