import React from 'react';

// Components
import StepNavigation from '../../Sidebar/StepNavigation';
import SkipStepNavigation from '../../Sidebar/SkipStepNavigation';
import PatientInfo from './Steps/PatientInfo';
import CarePlans from './Steps/CarePlans/CarePlans';
import Devices from './Steps/Devices/Devices';
import ShippingInfo from './Steps/ShippingInfo';
import Confirmation from './Steps/Confirmation';
import FinalizedOrder from './Steps/FinalizedOrder';

export default class Steps extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName:'',
      email:'',
      mobilePhone:'',
      bday:'',
      mrn:'',
      gender: 'None',
      language: 'None',
      shippingFirstName:'',
      shippingLastName:'',
      shippingPhone:'',
      countryCode:'',
      street:'',
      postalCode: '',
      country:'',
      city:'',
      state:'',
      deliveryPreference:'Deliver anytime',
      deviceSelected:0,
      devices: [
        {
          id: 0, 
          name: 'Body Scale', 
          img: require('../../../assets/scale.svg'), 
          description: 'Tactio 4G', 
          isSelected: false,
        },
        {
          id: 1, 
          name: 'Blood Pressure', 
          img: require('../../../assets/pressure-monitor.svg'), 
          description:'Tactio 4G', 
          isSelected: false,
        },
        // {id: 2, name: 'Blood Glucose', img: require('../../../assets/glucose-monitor.svg'), description:'Tactio 4G', isSelected: false}
      ],
      carePlanSelected: 0,
      carePlans: [
        {
          id: 'chf', 
          name: 'Congestive Heart Failure (CHF)', 
          availableFeatures: [
            'Reference Range (5)', 
            'Alerts (4)'
          ], 
          unavailableFeatures: [
            'Observations (6)', 
            'Messages', 
            'Questionnaires'
          ], 
          isSelected: false
        },
        {
          id: 'testP', 
          name: 'Test PLAN', 
          availableFeatures: [
            'Feature 1', 
            'Feature 2'
          ], 
          unavailableFeatures: [
            'Feature 3'
          ], 
          isSelected: false
        }
      ]
    };
  }

  handleOnChange = (e) => {
    const {name, value} = e.target
    this.setState({
      [name] : value
    });
  } 

  handleDeviceIsSelected = (e) => {
    let name = e.target.name;
    let {devices} = this.state;
    let newDevices = [];
    let deviceSelected = this.state.deviceSelected;

    for (let i in devices) {
      let device = devices[i];
      if(device.name === name) {
        device = {id: device.id, name: device.name, img: device.img, description: device.description, isSelected: !device.isSelected};
      if (device.isSelected){
          deviceSelected += 1;
        } else {
          deviceSelected -= 1;
        }
      }
      newDevices.push(device);
    }
    if(deviceSelected > 0) {
      this.props.onEnable(true);
    } else {
      this.props.onEnable(false);
    }
    this.setState({
      devices: newDevices,
      deviceSelected: deviceSelected
    });
  }


  handleCarePlanIsSelected = (e) => {
    let name = e.target.name;
    let {carePlans} = this.state;
    let newcarePlans = [];
    let carePlanSelected = this.state.carePlanSelected;

    for (let i in carePlans) {
      let carePlan = carePlans[i];
      if(carePlan.name === name) {
        carePlan = {id: carePlan.id, name: carePlan.name, availableFeatures: carePlan.availableFeatures, unavailableFeatures: carePlan.unavailableFeatures, isSelected: !carePlan.isSelected};
        if (carePlan.isSelected){
          carePlanSelected += 1;
        } else {
          carePlanSelected -= 1;
        }
      }
      newcarePlans.push(carePlan);
    }
    if(carePlanSelected > 0) {
      this.props.onEnable(true);
    } else {
      this.props.onEnable(false);
    }
    this.setState({
      carePlans: newcarePlans,
      carePlanSelected: carePlanSelected
    });
  }

  render() {
    let steps = null;
    if (this.props.currentStep < 2) {
      return null
    } 
    if (this.props.isFullSteps) {
      steps = (
        <section role="rowgroup"
               className="flex">
          <StepNavigation currentStep={this.props.currentStep} />
          <div className="s-col-6">
            <PatientInfo currentStep={this.props.currentStep}
                         firstName={this.state.firstName}
                         lastName={this.state.lastName}
                         email={this.state.email}
                         bday={this.state.bday}
                         mobilePhone={this.state.mobilePhone}
                         mrn={this.state.mrn}
                         gender={this.state.gender}
                         language={this.state.language}
                         onChange={this.handleOnChange}
                         onEnable={(val) => this.props.onEnable(val)}/>
            <CarePlans currentStep={this.props.currentStep} 
                       carePlans={this.state.carePlans} 
                       onChange={this.handleOnChange}
                       onSelect={this.handleCarePlanIsSelected}/>
            <Devices currentStep={this.props.currentStep}
                     devices={this.state.devices}
                     onSelect={this.handleDeviceIsSelected} />
            <ShippingInfo currentStep={this.props.currentStep}
                          devices={this.state.devices}
                          shippingFirstName={this.state.shippingFirstName}
                          shippingLastName={this.state.shippingLastName}
                          street={this.state.street}
                          shippingPhone={this.state.shippingPhone}
                          countryCode={this.state.countryCode}
                          postalCode={this.state.postalCode}
                          country={this.state.country}
                          state={this.state.state}
                          city={this.state.city}
                          onChange={this.handleOnChange}
                          onEnable={(val) => this.props.onEnable(val)}/>
            <Confirmation currentStep={this.props.currentStep}
                          devices={this.state.devices}
                          isFullSteps={this.props.isFullSteps}
                          group={this.props.group}
                          firstName={this.state.firstName}
                          lastName={this.state.lastName}
                          email={this.state.email}
                          bday={this.state.bday}
                          mobilePhone={this.state.mobilePhone}
                          mrn={this.state.mrn}
                          gender={this.state.gender}
                          language={this.state.language}
                          deliveryPreference={this.state.deliveryPreference}
                          shippingFirstName={this.state.shippingFirstName}
                          shippingLastName={this.state.shippingLastName}
                          street={this.state.street}
                          shippingPhone={this.state.shippingPhone}
                          countryCode={this.state.countryCode}
                          postalCode={this.state.postalCode}
                          country={this.state.country}
                          state={this.state.state}
                          city={this.state.city}
                          carePlans={this.state.carePlans}/>
            <FinalizedOrder currentStep={this.props.currentStep} 
                            isFullSteps={this.props.isFullSteps}
                            firstName={this.state.firstName}
                            lastName={this.state.lastName}/>
          </div>
        </section>
      );
    } else {
      steps = (
        <section role="rowgroup"
               className="flex">
          <SkipStepNavigation currentStep={this.props.currentStep} />
          <div className="s-col-6">
            <PatientInfo currentStep={this.props.currentStep}
                         firstName={this.state.firstName}
                         lastName={this.state.lastName}
                         email={this.state.email}
                         bday={this.state.bday}
                         mobilePhone={this.state.mobilePhone}
                         mrn={this.state.mrn}
                         gender={this.state.gender}
                         language={this.state.language}
                         onEnable={(val) => this.props.onEnable(val)}
                         onChange={this.handleOnChange}/>
            <CarePlans currentStep={this.props.currentStep} 
                       carePlans={this.state.carePlans}
                       onChange={this.handleOnChange}
                       onSelect={this.handleCarePlanIsSelected}/>
            <Confirmation currentStep={this.props.currentStep + 2}
                          isFullSteps={this.props.isFullSteps}
                          group={this.props.group}
                          firstName={this.state.firstName}
                          lastName={this.state.lastName}
                          email={this.state.email}
                          bday={this.state.bday}
                          mobilePhone={this.state.mobilePhone}
                          mrn={this.state.mrn}
                          gender={this.state.gender}
                          language={this.state.language}
                          carePlans={this.state.carePlans}/>
            <FinalizedOrder currentStep={this.props.currentStep + 2} 
                            isFullSteps={this.props.isFullSteps}
                            firstName={this.state.firstName}
                            lastName={this.state.lastName}/>
          </div>
        </section>
      );
    }
    return (
      <section role="row">
        {steps}
      </section>
    )
  } 
}