import React from 'react';

// Components
import Invitation from './Invitation';
import Steps from './Steps';
import ExitPageModal from '../../Modals/ExitPageModal'

export default class OnboardingMasterForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentStep: 1,
      visitedSteps: [],
      inviteBy: '',
      isNextEnabled: false,
      isFullSteps: true,
      group: 'Select a group',
    }
  }

  handleInvite = (val) => {
    this.setState({
        inviteBy: val
      });
  } 

  handleStepsStatus = (val) => {
    this.setState({
      isFullSteps: val === 'inviteOnly' ? false : true,
    });
  }

  handleOnGroup = (e) => {
    const {value} = e.target
    this.setState({
      group : value
    });
  } 

  handleNext = (currentStep) => {
    let isFullSteps = this.state.isFullSteps
    let enableValue = false;
    currentStep = currentStep >= 6? 7: currentStep + 1

    this.state.visitedSteps.map((step) => {
      if (step === currentStep) {
        enableValue = true;
      }
      return null; // mapping function expects a return value (i.e. not void)
    });

    this.setState({
      currentStep: currentStep,
      visitedSteps: [...this.state.visitedSteps, currentStep],
      isNextEnabled: (!isFullSteps && currentStep === 4) || (isFullSteps && currentStep === 6) || (enableValue) ? true : false,
    })
    
  }
    
  handlePrev = (currentStep) => {
    currentStep = currentStep <= 1? 1: currentStep - 1
    this.setState({
      currentStep: currentStep,
      isNextEnabled: true
    })
  }

  handleIsEnable = (val) => {
    this.setState({
      isNextEnabled: val,
    })
  }

  render() {
    let currentStep = this.state.currentStep;
    let max = 7;
    let maxMinusOne = 6;
    if (!this.state.isFullSteps) {
      max = 5;
      maxMinusOne = 4;
    }

    return (
      <section role="form"
               className="col-8 m-auto">
        <form className="my-xxxxxxxxxxl">
          <div className="col-11 ml-auto" >
            <Invitation currentStep={this.state.currentStep} 
                        onInvite={(val) => {this.handleInvite(val); this.handleStepsStatus(val);}}
                        onGroup={this.handleOnGroup}
                        onEnable={(val) => this.handleIsEnable(val)}
                        group={this.state.group}
                        inviteBy={this.state.inviteBy} />
          </div>  
          {this.props.isExitModalClicked ? <ExitPageModal onExitModalClicked={this.props.onExitModalClicked}/> : null}
          <Steps currentStep={this.state.currentStep}
                  group={this.state.group}
                  isFullSteps={this.state.isFullSteps}
                  onEnable={(val) => this.handleIsEnable(val)}/>
          <div role="navigation"
               className="ml-auto mt-xxxxl s-col-6">
            <div className="ml-xxxxxl">
              {currentStep !== 1 && currentStep < max &&
                <button className="btn btn-secondary float-left mb-xl" 
                        type="button" 
                        onClick={() => this.handlePrev(this.state.currentStep)} >
                  Previous
                </button>
              }
              {currentStep < max &&
                <button className="btn btn-primary float-right mb-xl" 
                        type="button"
                        onClick={() => this.handleNext(this.state.currentStep)}
                        disabled={!this.state.isNextEnabled} >
                  {currentStep === maxMinusOne ? "Confirm" : "Next"}
                </button> 
              }
            </div>
          </div>
        </form>
      </section>
    )
  }
}
