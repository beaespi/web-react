import React from 'react';

export default class SkipStepNavigation extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      steps: [
        'Patient information',
        'Care plan',
        'Review & Confirm'
      ],
    }
  }

  render() {

		return (
      <nav role="navigation"
           className="s-col-2 pl-0 pr-lrg mt-xxxxxxxl">
   			<p className="grey overline lh-1-65 mt-0 mb-lrg ls-1-5">Steps</p>
   			<ul className="list-unstyled" id={this.state.steps.length}>
			  	{this.state.steps.map((step, index) => (
	   				<li key={index}>
	   					<div className="flex align-items-center">
								<div className={`pulse-3s inline-block w-fixed-20 h-fixed-20 
																 ${this.props.currentStep === index + 2 && index === 0 ? 
																 		"border-rounded-light-blue" : 
																 		this.props.currentStep === index + 2 ? 
																 			"border2x-rounded-light-blue w-fixed-18 h-fixed-18" : 
																 			this.props.currentStep > index + 2 ? 
																 				"border2x-rounded-light-blue bg-blue-check w-fixed-18 h-fixed-18" : 
																 				"border-rounded-light-grey" }`} />
		   					<p className={`ease-0-15s pl-xxs m-0 
		   												 ${this.props.currentStep > index + 2 ? 
		   												 	"grey" : 
		   												 	this.props.currentStep === index + 2 ? 
		   												 		"grey fw-semibold" : 
		   												 		"light-grey"}`}>
									{step}
		   					</p>
		   				</div>
		   				{index !== 2 && 
	   						<div className="bg-light-grey w-fixed-1 h-fixed-36 ml-h-xxs" />
	   					}
	   				</li>
			  	))}
   			</ul>
      </nav>
		)
	}
}
