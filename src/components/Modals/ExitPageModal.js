import React from 'react';

// Components
import App from '../../App'

// Images
import exit from '../../assets/exit-popup.svg'


export default class ExitPageModal extends React.Component {
  
  handleQuit = (e) => {
    let name = e.target.name;
    if (name === 'quit'){
      return (
        <App />
      );
    } else {
      this.props.onExitModalClicked();
    }
    
  }

  render() {

    return (
      <div className="modal modal-content s-col-3">
        <button >
          <img src={exit} 
               alt="Exit"
               className="w-fixed-8 float-right"
               onClick={this.handleQuit} />
        </button>

        <div className="p-small align-items-center">
          <h6 className="grey lh-1-24 mt-auto mb-xs">Are you sure you want to quit?</h6>
          <p className="fs-small fw-regular grey lh-1-20">All progress in this session will be lost.</p>
        </div>

        <div className="float-right">
          <button className="btn btn-secondary"
                  name="quit"
                  onClick={this.handleQuit}> 
            Quit 
          </button>
          <button className="btn btn-primary"
                  name="back"
                  onClick={this.handleQuit}> 
            Back 
          </button>
        </div>
      </div>
    );
  }
}