import React from 'react';

export default class ProvisioningModuleHeader extends React.Component {

  render() {
    return (
      <header>
        <nav className="row col-8 m-auto justify-between align-items-center">
          <a href="/"
             className="no-decor pos-rel"
             title="go back to portal">
            <Tooltip>
              <img src={require('../../assets/logo.svg')} 
                   alt="Tactio"
                   className="h-fixed-25" />
            </Tooltip>
          </a>
        <button>
          <img src={require('../../assets/exit.svg')} 
               alt="Back to main menu"
               className="h-fixed-18"
               onClick={this.props.onExitModalClicked}  />
        </button>
        </nav>
      </header>
    )
  }
}


class Tooltip extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      displayTooltip: false
    }
  }

  hideTooltip = () => {
    this.setState({
      displayTooltip: false
    })  
  }
    
  showTooltip = () => {
    this.setState({
      displayTooltip: true
    })
  }

  render() {
    return (
      <span role="tooltip"
            onMouseLeave={this.hideTooltip}
            onMouseOver={this.showTooltip}>
        {this.state.displayTooltip &&
          <div className="bg-grey white caption px-lrg py-xxxs br-xxxs lh-1-25 pos-abs w-100 bottom-m-25 left-m-20 fade-in-5s">
            Go back to portal
          </div>
        }

        { // this.props.children == tooltip children in parent class
          this.props.children
        }
      </span>
    )    
  } 
}
