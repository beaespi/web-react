import React from 'react';
import './responsive-css/App.css';
import Header from './components/Header/ProvisioningModuleHeader.js';
import OnboardingForm from './components/Body/OnboardPatient/OnboardingMasterForm.js';

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isExitModalClicked: false,
    }
  }

  handleExitModalClicked = () => {
    this.setState({
      isExitModalClicked: !this.state.isExitModalClicked,
    })
  }

  render() {
    return (
    <div id="App"
    		 className="my-xxxxl">
    	<Header onExitModalClicked={this.handleExitModalClicked}/>
    	<OnboardingForm isExitModalClicked={this.state.isExitModalClicked}
                      onExitModalClicked={this.handleExitModalClicked}/>
    </div>

    )
  }
}
