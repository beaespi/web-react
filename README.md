## How to run the machine

In the root directory of `web-react`, run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.


### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!


## Compile SASS to CSS

In the root directory of `web-react`, run:

### `brew install sass/sass/sass`

Before editing the css, you must install sass first on your local.
If you don't have brew, install brew first before you continue.
View documention at [https://sass-lang.com/install](https://sass-lang.com/install).
Make sure to have node-sass installed. If you dont have it, run `npm install node-sass`.

<br><br>

In a new terminal run:

### `npm run sass`

This will enable auto-compiling of your sass to css.
